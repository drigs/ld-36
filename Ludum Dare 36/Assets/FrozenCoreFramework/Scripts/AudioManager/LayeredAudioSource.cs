﻿using UnityEngine;
using System.Collections;

namespace FrozenCore
{
    [RequireComponent(typeof(AudioSource))]
    public class LayeredAudioSource : MonoBehaviour
    {
        public AudioLayer audioLayer;
        public AudioSource audioSource { get; private set; } 

        public void Initialize()
        {
            audioSource = GetComponent<AudioSource>();
            AudioManager.Instance.AddSource(this);
        }

        void Start()
        {
            Initialize();
        }
    }
}