﻿using UnityEngine;
using System.Collections;
using FrozenCore;

public class UIAudioSlider : MonoBehaviour
{
    [SerializeField]
    AudioLayer layer;

    public void SetVolume(float volume)
    {
        AudioManager.Instance.SetLayerVolume(volume, layer);
    }    
}
