﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections;

namespace FrozenCore
{   
    public class AudioManager : MonoBehaviour
    {
        #region References
        [SerializeField] AudioClip[] audioClips;
        #endregion

        #region Attributes
        public static AudioManager Instance { get; private set; }

        List<AudioSource>[] audioLists;

        float[] volumes;
        #endregion

        #region Methods
        void Initialize()
        {
            Instance = this;

            int numOfLayers = (int)Enum.GetValues(typeof(AudioLayer)).Cast<AudioLayer>().Max() + 1;

            audioLists = new List<AudioSource>[numOfLayers];
            volumes = new float[numOfLayers];


            for (int i = 0; i < audioLists.Length; i++)
            {
                audioLists[i] = new List<AudioSource>();
                volumes [i] = 1f;
            }

            SetLayerVolume(.5f, AudioLayer.Ambient);
        }

        public void PlayLayer(AudioLayer layer)
        {
            foreach (AudioSource audioSource in GetList(layer))
            {
                audioSource.Play();
            }
        }

        public void PlayAllLayers()
        {
            foreach (var audioList in audioLists)
            {
                foreach (AudioSource audioSource in audioList)
                {
                    audioSource.Play();
                }
            }
        }
        /// <summary>
        /// Plays a clip with specified volume.
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="volume"></param>
        /// <param name="pitch"></param>
        public void PlayOnce(AudioClip clip, float volume = 1, float pitch = 1)
        {
            GameObject container = new GameObject(clip.name);

            container.transform.SetParent(transform);

            AudioSource source = container.AddComponent<AudioSource>();

            source.clip = clip;
            source.pitch = pitch;
            source.PlayOneShot(clip, volume);
            Destroy(container, clip.length / pitch);
        }

        /// <summary>
        /// Plays a clip among the given layer.
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="layer"></param>
        /// <param name="pitch"></param>
        public void PlayOnce(AudioClip clip, AudioLayer layer, float pitch = 1)
        {
            GameObject container = new GameObject(clip.name);

            container.transform.SetParent(transform);
            
            AudioSource source = container.AddComponent<AudioSource>();

            source.clip = clip;
            source.pitch = pitch;
            source.PlayOneShot(clip, volumes[(int)layer]);
            Destroy(container, clip.length / pitch);
        }

        public AudioSource PlayOnce(string clipName, AudioLayer layer, float pitch = 1)
        {
            GameObject container = new GameObject(clipName);

            container.transform.SetParent(transform);

            AudioSource source = container.AddComponent<AudioSource>();

            source.clip = GetClip(clipName);
            source.pitch = pitch;
            source.PlayOneShot(source.clip, volumes[(int)layer]);
            Destroy(container, source.clip.length / pitch);

            return source;
        }

        public void TweakMasterVolume(float tweak)
        {
            for (int i = 0; i < audioLists.Length; i++)
            {
                volumes[i] += tweak;

                for (int j = 0; j < audioLists[i].Count; j++)
                {
                    UpdateVolume(audioLists[i][j], i);
                }
            }
        }

        public void SetMasterVolume(float volume)
        {
            for (int i = 0; i < audioLists.Length; i++)
            {
                volumes[i] = volume;

                for (int j = 0; j < audioLists[i].Count; j++)
                {
                    UpdateVolume(audioLists[i][j], i);
                }
            }
        }

        public void TweakLayerVolume(float tweak, AudioLayer layer)
        {
            var audioList = GetList(layer);

            volumes[(int)layer] += tweak;

            for (int i = 0; i < audioList.Count; i++)
            {
                UpdateVolume(audioList[i], layer);
            }
        }

        public void SetLayerVolume(float volume, AudioLayer layer)
        {
            var audioList = GetList(layer);

            volumes[(int)layer] = volume;

            for (int i = 0; i < audioList.Count; i++)
            {
                UpdateVolume(audioList[i], layer);
            }
        }

        public void AddSource(LayeredAudioSource newSource)
        {
            UpdateVolume(newSource.audioSource, newSource.audioLayer);

            GetList(newSource.audioLayer).Add(newSource.audioSource);
        }        

        public void AddSource(AudioSource source, AudioLayer layer)
        {
            UpdateVolume(source, layer);

            GetList(layer).Add(source);
        }

        public void RemoveSource(LayeredAudioSource source)
        {
            GetList(source.audioLayer).Remove(source.audioSource);
        }

        public void RemoveSource(AudioSource source)
        {
            for (int i = 0; i < audioLists.Length; i++)
            {
                if (audioLists[i].Remove(source))
                {
                    break;
                }
            }
        }

        void UpdateVolume(AudioSource source, AudioLayer layer)
        {
            source.volume = volumes[(int)layer];
        }

        void UpdateVolume(AudioSource source, int layerIndex)
        {
            source.volume = volumes[layerIndex];
        }

        List<AudioSource> GetList(AudioLayer layer)
        {
            return audioLists[(int)layer];
        }

        AudioClip GetClip(string clipName)
        {
            foreach (AudioClip clip in audioClips)
            {
                if (clip.name == clipName)
                {
                    return clip;
                }
            }
            return null;
        }
        #endregion

        #region UnityMethods
        void Awake()
        {
            Initialize();
        }
        #endregion
    }
}
