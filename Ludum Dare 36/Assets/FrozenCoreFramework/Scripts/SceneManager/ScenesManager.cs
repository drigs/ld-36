﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace FrozenCore
{
    public class ScenesManager : MonoBehaviour {

        #region SceneTransition
        public enum SceneTransition
        {
            None, Fade, LoadingFade
        }
        #endregion

        #region Attributes
        static ScenesManager instance;
        public static ScenesManager Instance { get { return instance; } }

        [SerializeField]
        CanvasGroup cgpLoadingScreen;

        [SerializeField]
        Image imgLoading;
        #endregion

        #region Methods
        public void Initialize()
        {
            instance = this;
        }

        public void LoadScene(string scene, SceneTransition transition = SceneTransition.None)
        {
            switch (transition)
            {
                case SceneTransition.None:
                    SceneManager.LoadScene(scene);
                    break;
                case SceneTransition.Fade:
                case SceneTransition.LoadingFade:

                    cgpLoadingScreen.gameObject.SetActive(true);

                    if(transition == SceneTransition.Fade)
                    {
                        imgLoading.gameObject.SetActive(false);
                    }
                    else
                    {
                        imgLoading.gameObject.SetActive(true);
                    }

                    Tween fadeInTween = new Tween(0f, 1f, 1f, false, EasingType.Linear, delegate (float fadeValue)
                    {
                        cgpLoadingScreen.alpha = fadeValue;
                    });

                    fadeInTween.onComplete = delegate
                    {
                        StartCoroutine(AsyncSceneLoading(scene));
                    };

                    fadeInTween.DestroyOnComplete = true;
                    break;
                default:
                    break;
            }
        }

        private IEnumerator AsyncSceneLoading(string scene)
        {
            AsyncOperation sceneLoading = SceneManager.LoadSceneAsync(scene);
            yield return sceneLoading;

            Tween fadeOutTween = new Tween(1f, 0f, 1f, false, EasingType.Linear, delegate (float fadeValue)
            {
                cgpLoadingScreen.alpha = fadeValue;
            });

            fadeOutTween.onComplete = delegate
            {
                imgLoading.gameObject.SetActive(false);
                cgpLoadingScreen.gameObject.SetActive(false);
            };

            fadeOutTween.DestroyOnComplete = true;
        }
        #endregion
    }
}
