﻿using UnityEngine;
using System.Collections;

namespace FrozenCore
{
    public class CoreManager : MonoBehaviour {

        #region Attributes
        TweenManager tweenManager;

        AudioManager audioManager;

        [SerializeField]
        ScenesManager scenesManager;

        [SerializeField]
        EasingCurves[] easingCurves;
        #endregion

        #region Methods
        void Initialize()
        {
            tweenManager = new TweenManager();
            tweenManager.Initialize(easingCurves);

            scenesManager.Initialize();
        }
        #endregion

        #region Unity Methods
        void Awake()
        {
            //DontDestroyOnLoad(gameObject);

            Initialize();
        }

        void Update()
        {
            tweenManager.UpdateTweenManager();
        }

        void FixedUpdate()
        {
            tweenManager.FixedUpdateTweenManager();
        }
        #endregion
    }
}
