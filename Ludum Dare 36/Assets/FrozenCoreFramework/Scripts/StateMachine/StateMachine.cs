﻿using UnityEngine;
using System.Collections;

namespace FrozenCore
{
    public abstract class StateMachine<T> {

        #region Attributes
        MachineState<T>[] allStates;
        int currentState = -1;
        #endregion

        #region Methods
        public void SetStates(MachineState<T>[] allStates)
        {
            this.allStates = allStates;
        }

        public void ChangeState(int nextState)
        {
            if(currentState != nextState)
            {
                if(currentState >= 0)
                {
                    allStates[currentState].Exit();
                }

                currentState = nextState;

                allStates[currentState].Enter();
            }
        }
        #endregion
    }
}
