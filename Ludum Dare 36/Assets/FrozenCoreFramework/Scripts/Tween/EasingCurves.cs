﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EasingCurves {

    [SerializeField]
    EasingType type;

    [SerializeField]
    AnimationCurve curve;

    public EasingType Type { get { return type; } }
    public AnimationCurve Curve { get { return curve; } }
}
