﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FrozenCore;

public class SplashScreenManager : MonoBehaviour {

    [SerializeField]
    CanvasGroup splashScreenGroup;

	void Start()
    {
        Tween fadeInTween = new Tween(0, 1, 3, false, EasingType.Linear, delegate(float value)
        {
            splashScreenGroup.alpha = value;
        });

        fadeInTween.onComplete = delegate
        {
            StartCoroutine(FreezingTime());
        };
    }

    IEnumerator FreezingTime()
    {
        yield return new WaitForSeconds(2);

        Tween fadeOutTween = new Tween(1, 0, 2, false, EasingType.Linear, delegate (float value)
        {
            splashScreenGroup.alpha = value;
        });

        fadeOutTween.onComplete = delegate
        {
            ScenesManager.Instance.LoadScene("Menu");
        };
    }
}
