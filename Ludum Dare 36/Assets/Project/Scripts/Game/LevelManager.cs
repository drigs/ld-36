﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FrozenCore;

public class LevelManager : MonoBehaviour {

    static LevelManager instance;
    public static LevelManager Instance { get { return instance; } }

    [SerializeField]
    string nextLevel;

    void Awake()
    {
        instance = this;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Player.Instance.SetMove(false);

            ScenesManager.Instance.LoadScene(nextLevel, ScenesManager.SceneTransition.Fade);
        }
    }
}
