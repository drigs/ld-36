﻿using UnityEngine;
using System.Collections;
using FrozenCore;
using System;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class Portable : Highlightable
{
    public string title;

    public UnityEvent onGrab;

    Trigger frame;

    new Rigidbody rigidbody;

    float travelTime = 2f;

    bool isGrabbed;

    int collisions;

    new protected void Awake()
    {
        base.Awake();
        Initalize();
    }

    void Initalize()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    public Tween[] Place(Transform place, bool stayAtTarget)
    {
        Tween movement = MoveTo(place, travelTime);

        Tween rotation = RotateTo(place.transform.rotation, travelTime);

        if (stayAtTarget)
        {
            movement.onComplete += FinishPlacing;
            Player.Instance.SetGrabbedObject(null);
        }

        return new Tween[] { movement, rotation };
    }

    void FinishPlacing()
    {
        TweenAlpha(.5f, 1f, 1f);

        Unhighlight();

        isGrabbed = false;

        frame = GetComponentInParent<Trigger>();

        gameObject.layer = LayerMask.NameToLayer("Interactive");

        frame.Activate(this);        
    }

    void Grab()
    {
        if (Player.Instance.grabbedObject == null)
        {           
            rigidbody.constraints = RigidbodyConstraints.FreezeAll;

            rigidbody.useGravity = false;

            Unhighlight();

            Player.Instance.SetMove(false);

            Vector3 rot = Player.Instance.transform.rotation.eulerAngles;
            rot.y -= 270;
            RotateTo(Player.Instance.Hand.rotation, travelTime);

            MoveTo(Player.Instance.Hand, travelTime).onComplete += FinishGrab;
            
            gameObject.layer = LayerMask.NameToLayer("Default");

            if (onGrab != null)
            {
                onGrab.Invoke();
            }
        }
    }

    void TweenAlpha(float from, float to, float time)
    {
        Tween alphaTween = new Tween(from, to, time, false, EasingType.Linear, delegate (float a)
        {
            Color color = meshRenderer.material.color;
            color.a = a;
            meshRenderer.material.color = color;
        });

        alphaTween.DestroyOnComplete = true;
        alphaTween.StartTween();
    }

    void FinishGrab()
    {
        Player.Instance.SetGrabbedObject(this);

        TweenAlpha(1, .5f, 1);
                
        Player.Instance.SetMove(true);

        isGrabbed = true;

        if (frame)
        {
            frame.Deactivate();
            frame = null;
        }
    }

    public void Drop()
    {
        TweenAlpha(.5f, 1f, 1f);

        rigidbody.useGravity = true;

        isGrabbed = false;

        transform.SetParent(null);

        rigidbody.constraints = RigidbodyConstraints.None;

        gameObject.layer = LayerMask.NameToLayer("Interactive");
    }

    Tween MoveTo(Transform target, float duration)
    {
        Tween movement = new Tween(transform.position, target.position, duration, false, EasingType.Linear ,SetPosition);
        movement.DestroyOnComplete = true;
        movement.onComplete += delegate
        {
            SetPosition(target.position);
            transform.SetParent(target);            
        };

        movement.StartTween();

        return movement;
    }

    Tween RotateTo(Vector3 rotation, float duration)
    {
        Tween rotationTween = new Tween(transform.eulerAngles, rotation, duration, false, EasingType.Linear,SetEuler);
        rotationTween.DestroyOnComplete = true;
        rotationTween.StartTween();

        return rotationTween;
    }

    Tween RotateTo(Quaternion rotation, float duration)
    {
        Tween rotationTween = new Tween(transform.rotation, rotation, duration, false, EasingType.Linear, SetRotation);
        rotationTween.DestroyOnComplete = true;
        rotationTween.StartTween();

        return rotationTween;
    }

    void SetPosition(Vector3 newPosition)
    {
        transform.position = newPosition;
    }

    void SetEuler(Vector3 euler)
    {
        transform.eulerAngles = euler;
    }

    void SetRotation(Quaternion rotation)
    {
        transform.rotation = rotation;
    }

    public override void OnClick()
    {
        if (!isGrabbed)
            Grab();
    }

    public override void OnGazeEnter()
    {
        if (!isGrabbed)
            base.OnGazeEnter();
    }
}
