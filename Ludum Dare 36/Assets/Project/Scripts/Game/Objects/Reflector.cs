﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using FrozenCore;

public class Reflector : Highlightable
{
    [SerializeField]LightShaft lightShaftPrefab;
    
    public Transform pivot;

    Vector3 direction;

    LightShaft reflection;

    LightTrigger lastTrigger;

    LightTrigger trigger;

    AudioSource audioSource;

    bool rotating;

    new void Awake()
    {
        base.Awake();
        
        audioSource = GetComponent<AudioSource>();

        trigger = GetComponent<LightTrigger>();

        if (trigger)
        {
            trigger.onEnterShaft += (StartReflecting);
        }

        direction = transform.forward;
        reflection = GetComponent<LightShaft>();
    }

    void Update()
    {
        if (reflection)
        {
            direction = transform.forward;

            if (rotating)
            {
                reflection.CheckCollision();
            }
        }
    }
    
    public void StartReflecting(LightShaft origin)
    {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }

        if (!trigger.origin)
        {
            trigger.origin = origin;
            
            if (!reflection)
            {
                reflection = (LightShaft)Instantiate(lightShaftPrefab, pivot.position, transform.rotation, transform);
                
                reflection.destination.rotation = transform.rotation;
            }

            reflection.gameObject.SetActive(true);
        }
    }

    public void StopReflecting()
    {
        audioSource.Stop();

        if (reflection.nextTrigger != null)
        {
            if (reflection.nextTrigger != trigger)
            {
                if (reflection.nextTrigger.origin == reflection)
                {
                    reflection.nextTrigger.OnLightExit();
                }
            }
        }
        reflection.nextTrigger = null;
        reflection.gameObject.SetActive(false);

        trigger.origin = null;
    }
    
    public override void OnClick()
    {
        if (!rotating)
        {
            AudioManager.Instance.PlayOnce("Marble", AudioLayer.FX, UnityEngine.Random.Range(.85f, 1.15f));

            rotating = true;
            Tween tween = new Tween(transform.eulerAngles, transform.eulerAngles + Vector3.up * 90, 7.5f, false, EasingType.Linear, delegate (Vector3 rotation)
             {
                 transform.eulerAngles = rotation;
             });
            tween.DestroyOnComplete = true;
            tween.onComplete = delegate { rotating = false; };
        }
    }
}
