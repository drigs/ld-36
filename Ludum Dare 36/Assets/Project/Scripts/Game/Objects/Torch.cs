﻿using UnityEngine;
using System.Collections;
using FrozenCore;
public class Torch : MonoBehaviour
{
    public bool isInfinite;
    public FireLight fireLight;
    float duration = 60;

    void Start()
    {
        Initialize();
    }

    protected void Initialize()
    {
        if (!isInfinite)
        {
            StartCoroutine(IOscillate());

            PutOut();
        }
    }

    public void LightUp(float intensity)
    {
        fireLight.LightUp();

        Initialize();
    }

    public void SetInifinite(bool value)
    {
        isInfinite = value;
    }

    void PutOut()
    {
        Tween putOut = new Tween(fireLight.intensity, 0, duration, false, EasingType.Linear, delegate (float intensityInterpolation)
        {
            fireLight.intensity = intensityInterpolation;
        });
        putOut.DestroyOnComplete = true;
    }

    IEnumerator IOscillate()
    {
        yield return new WaitForSeconds(.05f);
        fireLight.Oscillate();

        if (fireLight.intensity > 0)
        {
            StartCoroutine(IOscillate());
        }
        else
        {
            fireLight.PutOut();
        }
    }
}
