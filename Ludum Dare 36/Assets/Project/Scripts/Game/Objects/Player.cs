﻿using UnityEngine;
using System.Collections;
using System;
using FrozenCore;

public class Player : MonoBehaviour
{
    #region References

    [SerializeField]AudioClip[] footsteps;
    [SerializeField]Transform hand;
    [SerializeField]LayerMask collisionMask;
    [SerializeField]LayerMask interactionMask;
    [SerializeField]LayerMask rotationMask;

    #endregion

    #region Attributes
    public static Player Instance { get; private set; }

    public Transform Hand { get { return hand; } }

    public Portable grabbedObject { get; set; }
    
    CharacterController cc;

    InteractiveObject target;

    float interactionDistance = 2.5f;

    BoxCollider boxChecker;
    
    //Movement
    public float speed = 5f;
    public float radius;
    public float height;
    float minDistance = 1f;
    bool canMove = true;
    //Audio
    AudioSource audioSource;

    #endregion

    #region Methods
    void Initialize()
    {
        Instance = this;

        SetGrabbedObject(hand.GetComponentInChildren<Portable>());

        audioSource = GetComponent<AudioSource>();

        cc = GetComponent<CharacterController>();

        radius = cc.radius;
        height = cc.height;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Awake()
    {
        Initialize();
    }

    void Update()
    {
        if (canMove)
        {
            //Movement
            float xMove = Input.GetAxisRaw("Horizontal");
            float zMove = Input.GetAxisRaw("Vertical");

            float maxDistance = grabbedObject ? DistanceFromHand() : minDistance;

            Vector3 movement = (transform.right * xMove + transform.forward * zMove).normalized;

            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, radius, Vector3.down, out hitInfo, height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);

            movement = Vector3.ProjectOnPlane(movement, hitInfo.normal).normalized;

            Vector3 direction = movement.normalized;

            movement = direction * speed * Time.fixedDeltaTime;
            movement.y -= 1f;

            
            //Debug.DrawRay(transform.position, direction * maxDistance, Color.red);

            float xRot = Input.GetAxis("Mouse Y");
            float yRot = Input.GetAxis("Mouse X");

            //Collision
            if (grabbedObject)
            {
                Collider objCollider = grabbedObject.GetComponent<Collider>();
                
                Vector3 p1 = transform.position + cc.center + Vector3.up * -cc.height * 0.5F;
                Vector3 p2 = p1 + Vector3.up * cc.height;

                if (direction.z > 0 || Mathf.Abs (direction.x) > 0)
                {
                    if (!Physics.CheckBox(objCollider.bounds.center + movement, objCollider.bounds.extents, grabbedObject.transform.localRotation, collisionMask))
                    //if (!Physics.CheckCapsule(transform.position, Hand.position + movement, objCollider.bounds.extents.x, collisionMask))
                    {
                        Walk(movement);
                    }
                }
                else
                {
                    Walk(movement);
                }
            }
            else
            {
                Ray ray = new Ray(transform.position, direction);
                if (!Physics.SphereCast(ray, .5f, minDistance, collisionMask))
                {
                    Walk(movement);
                }
            }

            //Rotation
            Quaternion camRot0 = Camera.main.transform.localRotation;
            Quaternion playerRot0 = transform.localRotation;

            Quaternion cameraRot = Camera.main.transform.localRotation * Quaternion.Euler(-xRot, 0, 0);
            Quaternion playerRot = transform.localRotation * Quaternion.Euler(0, yRot, 0);

            cameraRot = ClampRotationAroundXAxis(cameraRot);

            Rotate(cameraRot, playerRot);

            if (grabbedObject)
            {
                Collider objCollider = grabbedObject.GetComponent<Collider>();
                if (Physics.CheckBox(objCollider.bounds.center, objCollider.bounds.extents, transform.localRotation, rotationMask))
                {
                    Rotate(camRot0, playerRot0);
                }
            }
        }
        //Interaction
        RaycastHit interactionHit;
        if (Physics.SphereCast(Camera.main.transform.position, 1, Camera.main.transform.forward, out interactionHit, interactionDistance, interactionMask))
        {
            var newTarget = /*grabbedObject ? interactionHit.transform.GetComponent<Trigger>() : */interactionHit.transform.GetComponent<InteractiveObject>();

            if (newTarget != null)
            {
                if (newTarget != target)
                {
                    if (target != null)
                        target.OnGazeExit();

                    target = newTarget;
                    target.OnGazeEnter();
                }
            }
            //else
            //{
            //    CheckDrop();
            //}

            if (Input.GetMouseButtonDown(0))
            {
                if (target != null)
                {
                    target.OnClick();
                }
            }
        }
        else
        {
            //CheckDrop();

            if (target != null)
            {
                target.OnGazeExit();
                target = null;
            }
        }
    }

    public void SetGrabbedObject(Portable obj)
    {
        grabbedObject = obj;
        if (obj)
        {
            grabbedObject.gameObject.layer = gameObject.layer;            
        }
    }

    public float DistanceFromHand()
    {
        Vector3 maxDistance = grabbedObject ? hand.position + transform.forward * grabbedObject.GetComponent<Collider>().bounds.extents.z : hand.position;
        return Vector3.Distance(transform.position, maxDistance);
    }

    public void LockMovement(float time)
    {
        SetMove(false);
        Timer timer = new Timer(time, () => SetMove(true));
        timer.DestroyOnComplete();
    }

    public void SetMove(bool value)
    {
        canMove = value;
    }

    void Walk(Vector3 movement)
    {
        cc.Move(movement);
        if (Mathf.Abs(movement.x) > 0f || Mathf.Abs(movement.z) > 0f)
        {
            CycleFootsteps();
        }
    }

    void Rotate(Quaternion cameraRot, Quaternion playerRot)
    {
        transform.localRotation = playerRot;
        Camera.main.transform.localRotation = cameraRot;
    }

    void CycleFootsteps()
    {
        if (!audioSource.isPlaying)
        {
            int n = UnityEngine.Random.Range(1, footsteps.Length);

            audioSource.clip = footsteps[n];
            audioSource.PlayDelayed(1 / speed);

            footsteps[n] = footsteps[0];
            footsteps[0] = audioSource.clip;
        }
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, -90, 90);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }

    void CheckDrop()
    {
        if (grabbedObject)
        {
            if (Input.GetMouseButtonDown(1))
            {
                grabbedObject.Drop();
                grabbedObject = null;
            }
        }
    }
    #endregion
}
