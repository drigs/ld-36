﻿using UnityEngine;
using System.Collections;
using System;
using FrozenCore;

public class Lever : Highlightable
{
    [SerializeField]Vector3 finalRot;
    [SerializeField]bool repeatable;

    [SerializeField]
    SecretPassage secretPassage;

    Quaternion initialRot;
    bool canPull = true;

    public override void OnGazeEnter()
    {
        if (canPull)
        {
            base.OnGazeEnter();
        }
    }

    public override void OnClick()
    {
        if (canPull)
        {
            initialRot = transform.localRotation;

            AudioManager.Instance.PlayOnce("LeverPull", AudioLayer.FX, UnityEngine.Random.Range(.85f, 1.15f));

            Tween pullTween = new Tween(initialRot, Quaternion.Euler(finalRot), 3.5f, false, EasingType.LeverPull, SetRotation);

            canPull = false;

            pullTween.onComplete = delegate
            {
                AudioManager.Instance.PlayOnce("LeverCrack", AudioLayer.FX, UnityEngine.Random.Range(.85f, 1.15f));

                secretPassage.Open();

                if (repeatable)
                {
                    Tween moveBack = new Tween(Quaternion.Euler(finalRot), initialRot, 1f, false, EasingType.Linear, SetRotation);

                    moveBack.onComplete = delegate
                    {
                        canPull = true;
                    };
                }

                Unhighlight();
            };
        }
    }

    void SetRotation(Quaternion rot)
    {
        transform.localRotation = rot;
    }
}
