﻿using UnityEngine;
using System.Collections;
using System;

public class ButtonTrigger : Highlightable {

    [SerializeField]
    SecretPassage secretPassage;

    public override void OnClick()
    {
        secretPassage.Open();
    }
}
