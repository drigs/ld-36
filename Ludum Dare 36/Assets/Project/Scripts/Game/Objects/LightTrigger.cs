﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class LightTrigger : MonoBehaviour
{
    public UnityAction<LightShaft> onEnterShaft;
    public UnityEvent onEnter;
    public UnityEvent onExit;

    public LightShaft origin { get; set; }

    public void OnLightEnter(LightShaft origin)
    {
        if (onEnterShaft != null)
        {
            onEnterShaft.Invoke(origin);
        }
    }

    public void OnLightEnter()
    {
        if (onEnter != null)
        {
            onEnter.Invoke();
        }
    }

    public void OnLightExit()
    {
        if (onExit != null)
        {
            onExit.Invoke();
        }
    }
}
