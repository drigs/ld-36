﻿using UnityEngine;
using System.Collections;
using FrozenCore;

[RequireComponent(typeof(Light), typeof(LayeredAudioSource))]
public class FireLight : MonoBehaviour
{
    ParticleSystem[] systems;
    AudioSource audioSource;

    [SerializeField]
    Transform particlesParent;

    new Light light;

    public float intensity { get { return light.intensity; } set { light.intensity = value; } }

    float initialIntensity;

    float oscillation;

    void Initialize()
    {
        if (particlesParent)
        {
            systems = particlesParent.GetComponentsInChildren<ParticleSystem>();
        }
        light = GetComponent<Light>();
        initialIntensity = intensity = light.intensity;
        audioSource = GetComponent<AudioSource>();
    }

    public void Oscillate()
    {
        //oscillation = intensity / 10f;
        //light.intensity = Random.Range(intensity - oscillation, intensity + oscillation);
        
        foreach (ParticleSystem system in systems)
        {
            if (system.name != "Sparks")
            {
                system.startSize = intensity;
            }
            else
            {
                Random.Range(.005f, .015f);
            }
            system.startSpeed = intensity;
            system.startLifetime = Mathf.Lerp(intensity, 1, 0.5f);
        }
    }

    public void PutOut()
    {
        light.intensity = 0;

        oscillation = 0;

        audioSource.Stop();

        foreach (ParticleSystem system in systems)
        {            
            system.startSize = 0;
            system.startSpeed = 0;
            system.startLifetime = 0;
        }
    }

    public void LightUp()
    {
        AudioManager.Instance.PlayOnce("TorchLightUp", AudioLayer.FX, 1);
        audioSource.Play();
        intensity = initialIntensity;

        foreach (ParticleSystem system in systems)
        {
            if (system.name != "Sparks")
            {
                system.startSize = intensity;
            }
            else
            {
                Random.Range(.005f, .015f);
            }
            system.startSpeed = intensity;
            system.startLifetime = intensity;
        }
    }

    void Awake()
    {
        Initialize();
    }
}
