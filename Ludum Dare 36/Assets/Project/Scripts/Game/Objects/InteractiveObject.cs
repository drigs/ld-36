﻿using UnityEngine;
public interface InteractiveObject
{
    void OnGazeEnter();

    void OnGazeExit();

    void OnClick();
}