﻿using UnityEngine;
using System.Collections;
using FrozenCore;
using System;

public class SecretPassage : MonoBehaviour
{
    [SerializeField]
    Vector3 opennedPosition;

    [SerializeField]
    Vector3 closedPosition;

    bool isOpen;

    //void Start()
    //{
    //    LightTrigger lightTrigger = GetComponent<LightTrigger>();

    //    if (lightTrigger)
    //    {
    //        lightTrigger.onEnterShaft += (Open);
    //    }
    //}

    public void Open(LightShaft trigger)
    {
        if (!isOpen)
        {
            isOpen = true;

            var particles = GetComponentInChildren<ParticleSystem>();

            if (particles)
                particles.Play();

            Tween openTween = new Tween(closedPosition, opennedPosition, 4, false, EasingType.Linear, delegate (Vector3 value)
            {
                transform.localPosition = value;
            });

            openTween.DestroyOnComplete = true;

            AudioSource source = AudioManager.Instance.PlayOnce("ObjectMove", AudioLayer.FX, UnityEngine.Random.Range(.35f, .55f));

            openTween.onComplete = delegate
            {
                source.Stop();
                AudioManager.Instance.PlayOnce("LeverCrack", AudioLayer.FX, .5f);
            };
        }
    }

    public void Open()
    {
        if (!isOpen)
        {
            isOpen = true;

            var particles = GetComponentInChildren<ParticleSystem>();

            if (particles)
                particles.Play();

            Tween openTween = new Tween(closedPosition, opennedPosition, 8, false, EasingType.Linear, delegate (Vector3 value)
            {
                transform.localPosition = value;
                if (particles)
                {
                    if (Vector3.Distance(value, opennedPosition) < Vector3.Distance(closedPosition, opennedPosition) / 6)
                    {
                        particles.Stop();
                    }
                }
            });

            openTween.DestroyOnComplete = true;

            AudioSource source = AudioManager.Instance.PlayOnce("ObjectMove", AudioLayer.FX, UnityEngine.Random.Range(.35f, .55f));

            openTween.onComplete = delegate
            {
                source.Stop();
                AudioManager.Instance.PlayOnce("LeverCrack", AudioLayer.FX, .5f);
            };
        }
    }
}
