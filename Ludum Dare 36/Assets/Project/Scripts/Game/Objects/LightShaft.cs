﻿using UnityEngine;
using System.Collections;
using FrozenCore;
public class LightShaft : MonoBehaviour
{
    [SerializeField]
    LayerMask layerMask;

    public Transform destination;

    public new Light light;

    public LineRenderer shaft;

    public Vector3[] shaftPoints;

    public LightTrigger nextTrigger { get; set; }

    Vector3 lastHit;

    Transform lastObjectHit;

    Vector3 initialDestinationPoint;

    void Awake()
    {
        light = GetComponent<Light>();

        shaft = GetComponent<LineRenderer>();

        lastHit = destination.position;

        initialDestinationPoint = destination.localPosition;
    }

    void Update()
    {
        shaftPoints = new Vector3[] { transform.position, lastHit };

        light.range = Vector3.Distance(transform.position, lastHit);

        //shaft.SetWidth(0, 1);

        shaft.SetPositions(shaftPoints);

        CheckCollision();
    }

    public void CheckCollision()
    {
        RaycastHit col;

        if (Physics.SphereCast(transform.position, .5f, destination.position - transform.position, out col, 
            Vector3.Distance(transform.position, destination.position), layerMask, QueryTriggerInteraction.Ignore))
        //if (Physics.Raycast(transform.position, destination.position - transform.position, out col, 12f, layerMask, QueryTriggerInteraction.Ignore))
        {
            LightTrigger trigger = col.transform.GetComponent<LightTrigger>();

            if (trigger)
            {
                if (trigger != nextTrigger)
                {
                    trigger.OnLightEnter(this);
                    trigger.OnLightEnter();
                    nextTrigger = trigger;
                }
            }
            else
            {
                if (nextTrigger)
                {
                    if (nextTrigger.origin == this)
                    {
                        nextTrigger.OnLightExit();
                    }
                    nextTrigger = null;
                }

            }

            if (col.transform.position != lastHit || col.transform != lastObjectHit)
            {
                //if (col.transform.tag == "Reflector")
                //{
                //    Vector3 final = col.transform.position;
                //    final = col.transform.GetComponent<Reflector>().pivot.position;

                //    if (Mathf.Abs(transform.forward.x) < Mathf.Abs(transform.forward.z))
                //    {
                //        final.x = transform.position.x;
                //    }
                //    else
                //    {
                //        final.z = transform.position.z;
                //    }
                //    final.y = transform.position.y;
                //    lastHit = final;
                //}
                //else
                {
                    lastHit = col.point;
                    lastObjectHit = col.transform;
                }
            }
        }
        else
        {
            if (nextTrigger)
            {
                if (nextTrigger.origin == this)
                {
                    nextTrigger.OnLightExit();
                }
                nextTrigger = null;
            }

            lastHit = destination.position;
        }
    }

    //void OnTriggerEnter(Collider col)
    //{
    //    LightTrigger trigger = col.transform.GetComponent<LightTrigger>();

    //    if (trigger)
    //    {
    //        trigger.OnLightEnter();
    //        lastTrigger = trigger;
    //    }
    //}

    //void OnTriggerExit(Collider col)
    //{
    //    LightTrigger trigger = col.transform.GetComponent<LightTrigger>();
        
    //    if (trigger)
    //    {
    //        trigger.OnLightExit();
    //    }
    //}
}
