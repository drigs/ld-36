﻿using UnityEngine;
using System.Collections;
using System;
using FrozenCore;

public class Movable : Highlightable
{
    bool moving;

    public override void OnClick()
    {
        moving = true;

        Rigidbody rb = GetComponentInChildren<Rigidbody>();

        RigidbodyConstraints constraints = rb.constraints;
        rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;

        Vector3 forward = Player.Instance.transform.forward;

        if (Mathf.Abs(forward.z) > Mathf.Abs(forward.x))
        {
            forward = Vector3.forward * forward.z;
        }
        else
        {
            forward = Vector3.right * forward.x;
        }

        AudioManager.Instance.PlayOnce("ObjectMove", AudioLayer.FX);

        ParticleSystem particles = GetComponentInChildren<ParticleSystem>();

        particles.Play();

        Tween moveTween = new Tween(rb.velocity, forward * 2.5f, 7.5f, false, EasingType.Linear, delegate (Vector3 velocity)
        {
            rb.velocity = velocity;

            if (tag == "ReflectorBase")
            {
                Transform pyramid = transform.parent.FindChild("MarblePyramid");
                if (pyramid)
                {
                    pyramid.SetParent(transform);
                }
            }
        });

        moveTween.DestroyOnComplete = true;

        moveTween.onComplete = delegate 
        {
            rb.velocity = Vector3.zero;
            moving = false;
            particles.Stop();

            rb.constraints = constraints;

            if (tag == "Reflector")
            {
                Transform pyramid = transform.FindChild("MarblePyramid");
                pyramid.SetParent(transform.parent);
            }
        };

    }
}
