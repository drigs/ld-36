﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using FrozenCore;

public class Trigger : Highlightable
{
    public string expectedTitle;

    public UnityEvent onClick;

    public UnityEvent onEnd;

    bool occupied;

    bool triggering;

    public override void OnClick()
    {
        if (onClick != null)
        {
            onClick.Invoke();
        }
    }

    public override void OnGazeEnter()
    {
        if (!occupied)
        {
            if (Player.Instance.grabbedObject)
            {
                if (Player.Instance.grabbedObject.title == expectedTitle)
                {
                    base.OnGazeEnter();
                }
            }
        }
    }

    public void Activate(Portable obj)
    {
        occupied = true;
        meshRenderer.enabled = false;

        if (onEnd != null)
        {
            onEnd.Invoke();
            onEnd = null;
        }
    }

    public void Deactivate()
    {
        occupied = false;
        meshRenderer.enabled = true;
    }

    public void Place()
    {
        if (!occupied)
        {
            if (Player.Instance.grabbedObject)
            {
                if (Player.Instance.grabbedObject.title == expectedTitle)
                {
                    Player.Instance.grabbedObject.Place(transform, true);

                    Unhighlight();
                }
            }
        }
    }

    public void LightUpTorch()
    {
        if (!triggering)
        {
            if (Player.Instance.grabbedObject)
            {
                Torch torch = Player.Instance.grabbedObject.GetComponent<Torch>();
                if (torch)
                {
                    triggering = true;

                    Player.Instance.SetMove(false);

                    float placementTime = 3;

                    Transform placement = transform.FindChild("Lighting Place");

                    Vector3 initialPos = torch.transform.position;
                    Quaternion initialRot = torch.transform.rotation;

                    Tween rotTween = new Tween(initialRot, placement.rotation, placementTime + 2, false, EasingType.EaseOut, delegate (Quaternion rotation)
                    {
                        torch.transform.rotation = rotation;
                    });

                    Tween moveTween = new Tween(initialPos, placement.position, placementTime + 2, false, EasingType.EaseOut, delegate (Vector3 position)
                    {
                        torch.transform.position = position;
                    });

                    moveTween.onComplete = delegate
                    {
                        torch.LightUp(1);

                        Tween rotReturnTween = new Tween(placement.rotation, initialRot, placementTime , false, EasingType.EaseIn, delegate (Quaternion rotation)
                        {
                            torch.transform.rotation = rotation;
                        });

                        Tween moveReturnTween = new Tween(placement.position, initialPos, placementTime, false, EasingType.EaseIn, delegate (Vector3 position)
                        {
                            torch.transform.position = position;
                        });

                        moveReturnTween.onComplete = delegate
                        {
                            Player.Instance.SetMove(true);
                            triggering = false;
                        };
                    };
                }
            }
        }
    }
}
