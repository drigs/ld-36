﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Highlightable : MonoBehaviour, InteractiveObject
{
    [SerializeField]Color highlightColor = new Color(.2f, 0f, 0f);

    protected MeshRenderer meshRenderer;

    Color normalColor;

    bool highlighted;

    protected void Awake()
    {
        gameObject.layer = LayerMask.NameToLayer("Interactive");
        meshRenderer = GetComponent<MeshRenderer>();
        normalColor = meshRenderer.material.GetColor("_EmissionColor");
    }

    protected void Highlight()
    {
        meshRenderer.material.SetColor("_EmissionColor", highlightColor);
        highlighted = true;
    }

    protected void Unhighlight()
    {
        meshRenderer.material.SetColor("_EmissionColor", normalColor);
        highlighted = false;
     
    }

    public abstract void OnClick();

    public virtual void OnGazeEnter()
    {
        Highlight();
    }

    public virtual void OnGazeExit()
    {
        Unhighlight();
    }    
}
