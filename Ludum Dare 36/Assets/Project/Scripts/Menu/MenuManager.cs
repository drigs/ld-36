﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FrozenCore;

public class MenuManager : MonoBehaviour {

    [SerializeField]
    CanvasGroup[] canvasGroups;

    MenuStateMachine stateMachine;

    public enum MenuState
    {
        MainMenu, StartGame, HowToPlay, Credits
    }

	private class MenuStateMachine : StateMachine<MenuState>
    {
        public MenuStateMachine(CanvasGroup[] canvasGroups)
        {
            MachineState<MenuState>[] states = new MachineState<MenuState>[4] {
                new MainMenuState(canvasGroups[0]),
                new StartGameState(canvasGroups[1]),
                new HowToPlayState(canvasGroups[2]),
                new CreditsState(canvasGroups[3])
            };

            SetStates(states);
        }

        private class MainMenuState : MachineState<MenuState>
        {
            CanvasGroup mainMenuGroup;

            public MainMenuState(CanvasGroup group)
            {
                mainMenuGroup = group;
            }

            public override void Enter()
            {
                mainMenuGroup.gameObject.SetActive(true);
                mainMenuGroup.interactable = false;

                Tween fadeIn = new Tween(0, 1, 1, false, EasingType.Linear, delegate (float value)
                {
                    mainMenuGroup.alpha = value;
                });

                fadeIn.onComplete = delegate
                {
                    mainMenuGroup.interactable = true;
                };

                fadeIn.DestroyOnComplete = true;
            }

            public override void Exit()
            {
                mainMenuGroup.interactable = false;

                Tween fadeOut = new Tween(1, 0, 0.5f, false, EasingType.Linear, delegate (float value)
                {
                    mainMenuGroup.alpha = value;
                });

                fadeOut.onComplete = delegate
                {
                    mainMenuGroup.gameObject.SetActive(false);
                };

                fadeOut.DestroyOnComplete = true;
            }
        }

        private class CreditsState : MachineState<MenuState>
        {
            CanvasGroup creditsGroup;

            public CreditsState(CanvasGroup group)
            {
                creditsGroup = group;
            }

            public override void Enter()
            {
                creditsGroup.gameObject.SetActive(true);
                creditsGroup.interactable = false;

                Tween fadeIn = new Tween(0, 1, 1, false, EasingType.Linear, delegate (float value)
                {
                    creditsGroup.alpha = value;
                });

                fadeIn.onComplete = delegate
                {
                    creditsGroup.interactable = true;
                };

                fadeIn.DestroyOnComplete = true;
            }

            public override void Exit()
            {
                creditsGroup.interactable = false;

                Tween fadeOut = new Tween(1, 0, 0.5f, false, EasingType.Linear, delegate (float value)
                {
                    creditsGroup.alpha = value;
                });

                fadeOut.onComplete = delegate
                {
                    creditsGroup.gameObject.SetActive(false);
                };

                fadeOut.DestroyOnComplete = true;
            }
        }

        private class StartGameState : MachineState<MenuState>
        {
            CanvasGroup startGameGroup;

            public StartGameState(CanvasGroup group)
            {
                startGameGroup = group;
            }

            public override void Enter()
            {
                startGameGroup.gameObject.SetActive(true);

                Tween fadeIn = new Tween(0, 1, 1, false, EasingType.Linear, delegate (float value)
                {
                    startGameGroup.alpha = value;
                });

                fadeIn.DestroyOnComplete = true;
            }

            public override void Exit()
            {

            }
        }

        private class HowToPlayState : MachineState<MenuState>
        {
            CanvasGroup howToPlayGroup;

            public HowToPlayState(CanvasGroup group)
            {
                howToPlayGroup = group;
            }

            public override void Enter()
            {
                howToPlayGroup.gameObject.SetActive(true);
                howToPlayGroup.interactable = false;

                Tween fadeIn = new Tween(0, 1, 1, false, EasingType.Linear, delegate (float value)
                {
                    howToPlayGroup.alpha = value;
                });

                fadeIn.onComplete = delegate
                {
                    howToPlayGroup.interactable = true;
                };

                fadeIn.DestroyOnComplete = true;
            }

            public override void Exit()
            {
                howToPlayGroup.interactable = false;

                Tween fadeOut = new Tween(1, 0, 0.5f, false, EasingType.Linear, delegate (float value)
                {
                    howToPlayGroup.alpha = value;
                });

                fadeOut.onComplete = delegate
                {
                    howToPlayGroup.gameObject.SetActive(false);
                };

                fadeOut.DestroyOnComplete = true;
            }
        }
    }

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        stateMachine = new MenuStateMachine(canvasGroups);

        stateMachine.ChangeState(0);
    }

    public void ChangeToState(int state)
    {
        stateMachine.ChangeState(state);

        if(state == (int)MenuState.StartGame)
        {
            StartCoroutine(StartGameTimer());
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    public IEnumerator StartGameTimer()
    {
        yield return new WaitForSeconds(2);

        ScenesManager.Instance.LoadScene("Level1", ScenesManager.SceneTransition.Fade);
    }
}
